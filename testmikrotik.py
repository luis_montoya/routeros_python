from librouteros import connect
from librouteros.query import Key

api = connect(
    username='admin',
    password='abc123',
    host='192.168.12.1',
)
# Each key must be created first in order to reference it later.
name = Key('address')

for row in api.path('/ip/dhcp-server/lease').select(name):
   print(row)